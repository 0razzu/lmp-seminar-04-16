package certificate.model;


import certificate.error.CertificateErrorCode;
import certificate.error.CertificateException;
import certificate.util.CertificateFieldPattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Certificate {
    private String student;
    private String university;
    private String department;
    private String speciality;
    private String beginDate;
    private String endDate;
    private List<TableString> table;
    
    
    public Certificate(String student, String university, String department, String speciality,
                       String beginDate, String endDate, List<TableString> table) {
        setStudent(student);
        setUniversity(university);
        setDepartment(department);
        setSpeciality(speciality);
        setBeginDate(beginDate);
        setEndDate(endDate);
        setTable(table);
    }
    
    
    public Certificate(String student, String university, String department, String speciality,
                       String beginDate, String endDate) {
        this(student, university, department, speciality, beginDate, endDate, null);
    }
    
    
    public void setStudent(String student) {
        if (!CertificateFieldPattern.STUDENT.getPattern().matcher(student).matches())
            throw new CertificateException(CertificateErrorCode.INCORRECT_STUDENT);
        
        this.student = student;
    }
    
    
    public void setUniversity(String university) {
        if (!CertificateFieldPattern.UNIVERSITY.getPattern().matcher(university).matches())
            throw new CertificateException(CertificateErrorCode.INCORRECT_UNIVERSITY);
        
        this.university = university;
    }
    
    
    public void setDepartment(String department) {
        if (!CertificateFieldPattern.DEPARTMENT.getPattern().matcher(department).matches())
            throw new CertificateException(CertificateErrorCode.INCORRECT_DEPARTMENT);
        
        this.department = department;
    }
    
    
    public void setSpeciality(String speciality) {
        if (!CertificateFieldPattern.SPECIALITY.getPattern().matcher(speciality).matches())
            throw new CertificateException(CertificateErrorCode.INCORRECT_SPECIALITY);
        
        this.speciality = speciality;
    }
    
    
    public void setBeginDate(String beginDate) {
        if (!CertificateFieldPattern.DATE.getPattern().matcher(beginDate).matches())
            throw new CertificateException(CertificateErrorCode.INCORRECT_BEGIN_DATE);
        
        this.beginDate = beginDate;
    }
    
    
    public void setEndDate(String endDate) {
        if (!CertificateFieldPattern.DATE.getPattern().matcher(endDate).matches())
            throw new CertificateException(CertificateErrorCode.INCORRECT_END_DATE);
        
        this.endDate = endDate;
    }
    
    
    public void setTable(List<TableString> table) {
        if (table == null)
            this.table = new ArrayList<>();
        else
            this.table = table;
    }
    
    
    public String getStudent() {
        return student;
    }
    
    
    public String getUniversity() {
        return university;
    }
    
    
    public String getDepartment() {
        return department;
    }
    
    
    public String getSpeciality() {
        return speciality;
    }
    
    
    public String getBeginDate() {
        return beginDate;
    }
    
    
    public String getEndDate() {
        return endDate;
    }
    
    
    public List<TableString> getTable() {
        return table;
    }
    
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Certificate)) return false;
        Certificate that = (Certificate) o;
        return student.equals(that.student) &&
                university.equals(that.university) &&
                department.equals(that.department) &&
                speciality.equals(that.speciality) &&
                beginDate.equals(that.beginDate) &&
                endDate.equals(that.endDate) &&
                table.equals(that.table);
    }
    
    
    @Override
    public int hashCode() {
        return Objects.hash(student, university, department, speciality, beginDate, endDate, table);
    }
}
