package certificate.model;


import certificate.error.CertificateErrorCode;
import certificate.error.CertificateException;
import certificate.util.CertificateFieldPattern;

import java.util.Objects;


public class TableString {
    private String discipline;
    private int workload;
    private Mark mark;
    
    
    public TableString(String discipline, int workload, Mark mark) {
        setDiscipline(discipline);
        setWorkload(workload);
        setMark(mark);
    }
    
    
    public void setDiscipline(String discipline) {
        if (!CertificateFieldPattern.DISCIPLINE.getPattern().matcher(discipline).matches())
            throw new CertificateException(CertificateErrorCode.INCORRECT_DISCIPLINE);
        
        this.discipline = discipline;
    }
    
    
    public void setWorkload(int workload) {
        if (workload <= 0)
            throw new CertificateException(CertificateErrorCode.NON_POSITIVE_WORKLOAD);
        
        this.workload = workload;
    }
    
    
    public void setMark(Mark mark) {
        this.mark = mark;
    }
    
    
    public String getDiscipline() {
        return discipline;
    }
    
    
    public int getWorkload() {
        return workload;
    }
    
    
    public Mark getMark() {
        return mark;
    }
    
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TableString)) return false;
        TableString string = (TableString) o;
        return workload == string.workload &&
                discipline.equals(string.discipline) &&
                mark == string.mark;
    }
    
    
    @Override
    public int hashCode() {
        return Objects.hash(discipline, workload, mark);
    }
}
