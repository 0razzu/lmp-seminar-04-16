package certificate.model;


public enum Mark {
    SET_OFF_FAIL(0, "Не зачтено"),
    SET_OFF_PASS(1, "Зачтено"),
    EXAM_FAIL(2, "Неудовлетворительно"),
    EXAM_PASS(3, "Удовлетворительно"),
    EXAM_PASS_WITH_MERIT(4, "Хорошо"),
    EXAM_PASS_WITH_DISTINCTION(5, "Отлично");
    
    
    private int value;
    private String representation;
    
    
    Mark(int value, String representation) {
        this.value = value;
        this.representation = representation;
    }
    
    
    public int getValue() {
        return value;
    }
    
    
    public String getRepresentation() {
        return representation;
    }
}
