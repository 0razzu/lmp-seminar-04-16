package certificate.error;


public enum CertificateErrorCode {
    INCORRECT_DISCIPLINE("Поле «Дисциплина» заполнено некорректно"),
    NON_POSITIVE_WORKLOAD("Значение трудоёмкости должно быть положительным"),
    INCORRECT_STUDENT("Поле «ФИО студента» заполнено некорректно"),
    INCORRECT_UNIVERSITY("Поле «Университет» заполнено некорректно"),
    INCORRECT_DEPARTMENT("Поле «Факультет» заполнено некорректно"),
    INCORRECT_SPECIALITY("Поле «Специальность» заполнено некорректно"),
    INCORRECT_BEGIN_DATE("Поле «Дата начала обучения» заполнено некорректно"),
    INCORRECT_END_DATE("Поле «Дата окончания обучения» заполнено некорректно");
    
    
    private String errorString;
    
    
    CertificateErrorCode(String errorString) {
        this.errorString = errorString;
    }
    
    
    public String getErrorString() {
        return errorString;
    }
}
