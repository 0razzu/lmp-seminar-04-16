package certificate.error;


public class CertificateException extends IllegalArgumentException {
    private CertificateErrorCode errorCode;
    
    
    public CertificateException(CertificateErrorCode errorCode) {
        this.errorCode = errorCode;
    }
    
    
    public CertificateErrorCode getErrorCode() {
        return errorCode;
    }
    
    
    @Override
    public String getMessage() {
        return errorCode.getErrorString();
    }
}
