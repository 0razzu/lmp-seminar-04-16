package certificate.service;


import certificate.model.Certificate;
import certificate.model.Mark;
import certificate.model.TableString;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


public class TestCertificateService {
    private static final double EPS = 1E-6;
    private static final Certificate certificate1 = new Certificate("Иван Иванов", "ОмГУ", "ИМИТ", "Прикл. мат. и инф.",
            "01.09.2019", "31.08.2022");
    private static final Certificate certificate2 = new Certificate("Анастасия Петрова", "ОмГТУ", "РТФ", "Наноинженерия",
            "01.09.2018", "31.08.2021");
    private static final Certificate certificate3 = new Certificate("Пётр Иванов", "ОмГУ", "Хим. факультет", "Химия",
            "01.09.2020", "31.08.2024");
    private static final Certificate certificate4 = new Certificate("Анна Ю", "ОмГПУ", "Сервис и туризм", "Туризм",
            "01.09.2015", "31.08.2019");
    
    
    @BeforeAll
    static void initCertificate1() {
        certificate1.getTable().add(new TableString("ДММЛ", 216, Mark.EXAM_PASS_WITH_DISTINCTION));
        certificate1.getTable().add(new TableString("Алгебра", 512, Mark.EXAM_PASS_WITH_DISTINCTION));
        certificate1.getTable().add(new TableString("Мат. анализ", 512, Mark.EXAM_FAIL));
        certificate1.getTable().add(new TableString("Физкультура", 5120, Mark.SET_OFF_FAIL));
        certificate1.getTable().add(new TableString("БЖД", 110, Mark.SET_OFF_PASS));
        certificate1.getTable().add(new TableString("История", 108, Mark.EXAM_PASS));
    }
    
    
    @BeforeAll
    static void initCertificate2() {
        certificate2.getTable().add(new TableString("Математика", 512, Mark.EXAM_FAIL));
        certificate2.getTable().add(new TableString("Физика", 1022, Mark.EXAM_PASS));
        certificate2.getTable().add(new TableString("Физкультура", 5120, Mark.SET_OFF_PASS));
        certificate2.getTable().add(new TableString("БЖД", 110, Mark.SET_OFF_PASS));
        certificate2.getTable().add(new TableString("История", 108, Mark.EXAM_PASS_WITH_MERIT));
    }
    
    
    @BeforeAll
    static void initCertificate4() {
        certificate4.getTable().add(new TableString("Физкультура", 5500, Mark.SET_OFF_FAIL));
    }
    
    
    @Test
    void testGetDisciplines() {
        List<String> disciplines1 = new ArrayList<>(6);
        List<String> disciplines2 = new ArrayList<>(5);
        List<String> disciplines3 = new ArrayList<>(0);
        List<String> disciplines4 = new ArrayList<>(1);
        
        disciplines1.add("ДММЛ");
        disciplines1.add("Алгебра");
        disciplines1.add("Мат. анализ");
        disciplines1.add("Физкультура");
        disciplines1.add("БЖД");
        disciplines1.add("История");
        
        disciplines2.add("Математика");
        disciplines2.add("Физика");
        disciplines2.add("Физкультура");
        disciplines2.add("БЖД");
        disciplines2.add("История");
        
        disciplines4.add("Физкультура");
        
        assertAll(
                () -> assertEquals(disciplines1, CertificateService.getDisciplines(certificate1)),
                () -> assertEquals(disciplines2, CertificateService.getDisciplines(certificate2)),
                () -> assertEquals(disciplines3, CertificateService.getDisciplines(certificate3)),
                () -> assertEquals(disciplines4, CertificateService.getDisciplines(certificate4))
        );
    }
    
    
    @Test
    void testGetSumWorkload() {
        assertAll(
                () -> assertEquals(6578, CertificateService.getSumWorkload(certificate1)),
                () -> assertEquals(6872, CertificateService.getSumWorkload(certificate2)),
                () -> assertEquals(0, CertificateService.getSumWorkload(certificate3)),
                () -> assertEquals(5500, CertificateService.getSumWorkload(certificate4))
        );
    }
    
    
    @Test
    void testGetAverageMark() {
        assertAll(
                () -> assertEquals(13. / 3, CertificateService.getAverageMark(certificate1), EPS),
                () -> assertEquals(3.5, CertificateService.getAverageMark(certificate2), EPS),
                () -> assertEquals(0, CertificateService.getAverageMark(certificate3), EPS),
                () -> assertEquals(0, CertificateService.getAverageMark(certificate4), EPS)
        );
    }
    
    
    @Test
    void testGetDisciplineMarkMap() {
        Map<String, String> disciplineMarkMap1 = new HashMap<>(6);
        Map<String, String> disciplineMarkMap2 = new HashMap<>(5);
        Map<String, String> disciplineMarkMap3 = new HashMap<>(0);
        Map<String, String> disciplineMarkMap4 = new HashMap<>(1);
        
        disciplineMarkMap1.put("ДММЛ", "Отлично");
        disciplineMarkMap1.put("Алгебра", "Отлично");
        disciplineMarkMap1.put("Мат. анализ", "Неудовлетворительно");
        disciplineMarkMap1.put("Физкультура", "Не зачтено");
        disciplineMarkMap1.put("БЖД", "Зачтено");
        disciplineMarkMap1.put("История", "Удовлетворительно");
        
        disciplineMarkMap2.put("Математика", "Неудовлетворительно");
        disciplineMarkMap2.put("Физика", "Удовлетворительно");
        disciplineMarkMap2.put("Физкультура", "Зачтено");
        disciplineMarkMap2.put("БЖД", "Зачтено");
        disciplineMarkMap2.put("История", "Хорошо");
        
        disciplineMarkMap4.put("Физкультура", "Не зачтено");
        
        assertAll(
                () -> assertEquals(disciplineMarkMap1, CertificateService.getDisciplineMarkMap(certificate1)),
                () -> assertEquals(disciplineMarkMap2, CertificateService.getDisciplineMarkMap(certificate2)),
                () -> assertEquals(disciplineMarkMap3, CertificateService.getDisciplineMarkMap(certificate3)),
                () -> assertEquals(disciplineMarkMap4, CertificateService.getDisciplineMarkMap(certificate4))
        );
    }
    
    
    @Test
    void testGetMarkDisciplineMap() {
        Map<String, List<String>> markDisciplineMap1 = CertificateService.getMarkDisciplineMap(certificate1);
        Map<String, List<String>> markDisciplineMap2 = CertificateService.getMarkDisciplineMap(certificate2);
        Map<String, List<String>> markDisciplineMap3 = CertificateService.getMarkDisciplineMap(certificate3);
        Map<String, List<String>> markDisciplineMap4 = CertificateService.getMarkDisciplineMap(certificate4);
        
        assertAll(
                () -> assertEquals(Arrays.asList("ДММЛ", "Алгебра"), markDisciplineMap1.get("Отлично")),
                () -> assertTrue(markDisciplineMap1.get("Хорошо").isEmpty()),
                () -> assertEquals(Collections.singletonList("История"), markDisciplineMap1.get("Удовлетворительно")),
                () -> assertEquals(Collections.singletonList("Мат. анализ"), markDisciplineMap1.get("Неудовлетворительно")),
                () -> assertEquals(Collections.singletonList("БЖД"), markDisciplineMap1.get("Зачтено")),
                () -> assertEquals(Collections.singletonList("Физкультура"), markDisciplineMap1.get("Не зачтено"))
        );
        
        assertAll(
                () -> assertTrue(markDisciplineMap2.get("Отлично").isEmpty()),
                () -> assertEquals(Collections.singletonList("История"), markDisciplineMap2.get("Хорошо")),
                () -> assertEquals(Collections.singletonList("Физика"), markDisciplineMap2.get("Удовлетворительно")),
                () -> assertEquals(Collections.singletonList("Мат. анализ"), markDisciplineMap1.get("Неудовлетворительно")),
                () -> assertEquals(Arrays.asList("Физкультура", "БЖД"), markDisciplineMap2.get("Зачтено")),
                () -> assertTrue(markDisciplineMap2.get("Не зачтено").isEmpty())
        );
        
        assertAll(
                () -> assertTrue(markDisciplineMap3.get("Отлично").isEmpty()),
                () -> assertTrue(markDisciplineMap3.get("Хорошо").isEmpty()),
                () -> assertTrue(markDisciplineMap3.get("Удовлетворительно").isEmpty()),
                () -> assertTrue(markDisciplineMap3.get("Неудовлетворительно").isEmpty()),
                () -> assertTrue(markDisciplineMap3.get("Зачтено").isEmpty()),
                () -> assertTrue(markDisciplineMap3.get("Не зачтено").isEmpty())
        );
        
        assertAll(
                () -> assertTrue(markDisciplineMap4.get("Отлично").isEmpty()),
                () -> assertTrue(markDisciplineMap4.get("Хорошо").isEmpty()),
                () -> assertTrue(markDisciplineMap4.get("Удовлетворительно").isEmpty()),
                () -> assertTrue(markDisciplineMap4.get("Неудовлетворительно").isEmpty()),
                () -> assertTrue(markDisciplineMap4.get("Зачтено").isEmpty()),
                () -> assertEquals(Collections.singletonList("Физкультура"), markDisciplineMap1.get("Не зачтено"))
        );
    }
}
