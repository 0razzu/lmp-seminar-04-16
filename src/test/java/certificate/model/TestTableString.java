package certificate.model;


import certificate.error.CertificateErrorCode;
import certificate.error.CertificateException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class TestTableString {
    @Test
    void testTableStringExam() {
        TableString string = new TableString("Мат. анализ", 756, Mark.EXAM_FAIL);
        
        assertAll(
                () -> assertEquals("Мат. анализ", string.getDiscipline()),
                () -> assertEquals(756, string.getWorkload()),
                () -> assertEquals(Mark.EXAM_FAIL, string.getMark())
        );
    }
    
    
    @Test
    void testTableStringSetOff() {
        TableString string = new TableString("БЖД", 108, Mark.SET_OFF_PASS);
        
        assertAll(
                () -> assertEquals("БЖД", string.getDiscipline()),
                () -> assertEquals(108, string.getWorkload()),
                () -> assertEquals(Mark.SET_OFF_PASS, string.getMark())
        );
    }
    
    
    @Test
    void testExceptions() {
        assertThrows(NullPointerException.class, () -> new TableString(null, 100, Mark.SET_OFF_PASS));
        
        try {
            TableString string = new TableString(".Абв", 100, Mark.EXAM_PASS);
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_DISCIPLINE, e.getErrorCode());
        }
        
        try {
            TableString string = new TableString("Программирование на C++", 0, Mark.EXAM_PASS_WITH_DISTINCTION);
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.NON_POSITIVE_WORKLOAD, e.getErrorCode());
        }
    }
    
    
    @Test
    void testEquals() {
        TableString string1 = new TableString("Диф. уравнения", 756, Mark.EXAM_PASS_WITH_DISTINCTION);
        TableString string2 = new TableString("Диф. уравнения", 756, Mark.EXAM_PASS_WITH_DISTINCTION);
        TableString string3 = new TableString("Программирование на C++", 756, Mark.EXAM_PASS_WITH_DISTINCTION);
        TableString string4 = new TableString("Диф. уравнения", 760, Mark.EXAM_PASS_WITH_DISTINCTION);
        TableString string5 = new TableString("Диф. уравнения", 756, Mark.EXAM_PASS_WITH_MERIT);
        
        assertAll(
                () -> assertEquals(string1, string1),
                () -> assertEquals(string1, string2),
                () -> assertEquals(string2, string1),
                () -> assertNotEquals(string1, ""),
                () -> assertNotEquals(string1, string3),
                () -> assertNotEquals(string1, string4),
                () -> assertNotEquals(string1, string5),
                () -> assertNotEquals(string4, string5)
        );
    }
}
