package certificate.model;


import certificate.error.CertificateErrorCode;
import certificate.error.CertificateException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class TestCertificate {
    @Test
    void testCertificate1() {
        List<TableString> table = new ArrayList<>(2);
        table.add(new TableString("Непонятный предмет 1", 1024, Mark.EXAM_PASS_WITH_MERIT));
        table.add(new TableString("Непонятный предмет 2", 4096, Mark.SET_OFF_FAIL));
        
        Certificate certificate = new Certificate("Иван Петрович Иванов", "Какой-то типа универ",
                "Факультет непонятно чего", "Не нужная никому специальность", "01.09.2015", "29.02.2020", table);
        
        assertAll(
                () -> assertEquals("Иван Петрович Иванов", certificate.getStudent()),
                () -> assertEquals("Какой-то типа универ", certificate.getUniversity()),
                () -> assertEquals("Факультет непонятно чего", certificate.getDepartment()),
                () -> assertEquals("Не нужная никому специальность", certificate.getSpeciality()),
                () -> assertEquals("01.09.2015", certificate.getBeginDate()),
                () -> assertEquals("29.02.2020", certificate.getEndDate()),
                () -> assertEquals(table, certificate.getTable())
        );
    }
    
    
    @Test
    void testCertificate2() {
        Certificate certificate = new Certificate("Кто-то со странным именем с кучей слов и букав Алексей",
                "Какой-то типа универ, в названии которого зачем-то «кавычечки». И циферки, куда без них 123",
                "Факультет непонятно чего, где есть точки. А ещё дефисы зачем-то",
                "51.03.06 Не нужная никому специальность. И циферки тут, конечно же, но только в начале",
                "29.02.0800", "12.02.2023");
        
        assertAll(
                () -> assertEquals("Кто-то со странным именем с кучей слов и букав Алексей", certificate.getStudent()),
                () -> assertEquals("Какой-то типа универ, в названии которого зачем-то «кавычечки». " +
                        "И циферки, куда без них 123", certificate.getUniversity()),
                () -> assertEquals("Факультет непонятно чего, где есть точки. А ещё дефисы зачем-то",
                        certificate.getDepartment()),
                () -> assertEquals("51.03.06 Не нужная никому специальность. И циферки тут, конечно же, но только в начале",
                        certificate.getSpeciality()),
                () -> assertEquals("29.02.0800", certificate.getBeginDate()),
                () -> assertEquals("12.02.2023", certificate.getEndDate()),
                () -> assertEquals(new ArrayList<>(), certificate.getTable())
        );
    }
    
    
    @Test
    void testCertificate3() {
        Certificate certificate = new Certificate("Ахмед Октай оглы Алиев", "СибАДИ",
                "Экономика и управление", "38.03.02 Менеджмент", "08.11.2018", "10.03.2019", null);
        
        assertAll(
                () -> assertEquals("Ахмед Октай оглы Алиев", certificate.getStudent()),
                () -> assertEquals("СибАДИ", certificate.getUniversity()),
                () -> assertEquals("Экономика и управление", certificate.getDepartment()),
                () -> assertEquals("38.03.02 Менеджмент", certificate.getSpeciality()),
                () -> assertEquals("08.11.2018", certificate.getBeginDate()),
                () -> assertEquals("10.03.2019", certificate.getEndDate()),
                () -> assertEquals(new ArrayList<>(), certificate.getTable())
        );
    }
    
    
    @Test
    void testExceptionsStudent() {
        try {
            Certificate certificate = new Certificate("с маленькой буквы", "ОмГУ", "ИМИТ",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_STUDENT, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Цифры 123", "ОмГУ", "ИМИТ",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_STUDENT, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Латинница Abc", "ОмГУ", "ИМИТ",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_STUDENT, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("А!!!", "ОмГУ", "ИМИТ",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_STUDENT, e.getErrorCode());
        }
    }
    
    
    @Test
    void testExceptionsUniversity() {
        try {
            Certificate certificate = new Certificate("Студент какой-то", "с маленькой буквы", "ИМИТ",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_UNIVERSITY, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", ".ОмГУ", "ИМИТ",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_UNIVERSITY, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ aka OmSU", "ИМИТ",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_UNIVERSITY, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "#", "ИМИТ",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_UNIVERSITY, e.getErrorCode());
        }
    }
    
    
    @Test
    void testExceptionsDepartment() {
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "с маленькой",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_DEPARTMENT, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "IMIT",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_DEPARTMENT, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "--",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_DEPARTMENT, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "Цифры 10",
                    "Прикладная математика и информатика", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_DEPARTMENT, e.getErrorCode());
        }
    }
    
    
    @Test
    void testExceptionsSpeciality() {
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "01.02.01А пробел где", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_SPECIALITY, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "А вопросительные знаки нельзя?", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_SPECIALITY, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "02.01.01", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_SPECIALITY, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "No English", "01.09.2020", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_SPECIALITY, e.getErrorCode());
        }
    }
    
    
    @Test
    void testExceptionsBeginDate() {
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "Абв", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_BEGIN_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "00.10.2011", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_BEGIN_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.00.2000", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_BEGIN_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.08.0000", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_BEGIN_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "1.11.2015", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_BEGIN_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.11.15", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_BEGIN_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "29.02.2017", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_BEGIN_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "29.02.1700", "31.08.2024");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_BEGIN_DATE, e.getErrorCode());
        }
    }
    
    
    @Test
    void testExceptionsEndDate() {
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.09.2018", "Абв");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_END_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.09.2018", "00.09.2020");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_END_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.09.2018", "01.00.2019");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_END_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.09.2018", "01.08.0000");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_END_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.09.2018", "1.11.2022");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_END_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.09.2018", "01.11.22");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_END_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.09.2018", "29.02.2001");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_END_DATE, e.getErrorCode());
        }
        
        try {
            Certificate certificate = new Certificate("Студент какой-то", "ОмГУ", "ИМИТ",
                    "Прикладное что-то там", "01.09.2018", "29.02.1800");
            fail();
        } catch (CertificateException e) {
            assertEquals(CertificateErrorCode.INCORRECT_END_DATE, e.getErrorCode());
        }
    }
    
    
    @Test
    void testEquals() {
        List<TableString> table1 = new ArrayList<>(1);
        table1.add(new TableString("Алгебра и геометрия", 1024, Mark.EXAM_PASS_WITH_MERIT));
        
        List<TableString> table2 = new ArrayList<>(1);
        table2.add(new TableString("Физкультура", 65536, Mark.SET_OFF_PASS));
        
        Certificate certificate1 = new Certificate("Иван Петрович Иванов", "ОмГУ", "ИМИТ",
                "Прикл. мат. и инф.", "01.09.2015", "29.02.2016", table1);
        Certificate certificate2 = new Certificate("Иван Петрович Иванов", "ОмГУ", "ИМИТ",
                "Прикл. мат. и инф.", "01.09.2015", "29.02.2016", table1);
        Certificate certificate3 = new Certificate("Иван Петрович Неиванов", "ОмГУ", "ИМИТ",
                "Прикл. мат. и инф.", "01.09.2015", "29.02.2016", table1);
        Certificate certificate4 = new Certificate("Иван Петрович Иванов", "Секретный Университет", "ИМИТ",
                "Прикл. мат. и инф.", "01.09.2015", "29.02.2016", table1);
        Certificate certificate5 = new Certificate("Иван Петрович Иванов", "ОмГУ", "Физфак",
                "Прикл. мат. и инф.", "01.09.2015", "29.02.2016", table1);
        Certificate certificate6 = new Certificate("Иван Петрович Иванов", "ОмГУ", "ИМИТ",
                "Фунд. мат. и мех.", "01.09.2015", "29.02.2016", table1);
        Certificate certificate7 = new Certificate("Иван Петрович Иванов", "ОмГУ", "ИМИТ",
                "Прикл. мат. и инф.", "01.09.2014", "29.02.2016", table1);
        Certificate certificate8 = new Certificate("Иван Петрович Иванов", "ОмГУ", "ИМИТ",
                "Прикл. мат. и инф.", "01.09.2015", "27.02.2016", table1);
        Certificate certificate9 = new Certificate("Иван Петрович Иванов", "ОмГУ", "ИМИТ",
                "Прикл. мат. и инф.", "01.09.2015", "29.02.2016", table2);
        
        assertAll(
                () -> assertEquals(certificate1, certificate1),
                () -> assertEquals(certificate1, certificate2),
                () -> assertEquals(certificate2, certificate1),
                () -> assertNotEquals(certificate1, certificate3),
                () -> assertNotEquals(certificate1, certificate4),
                () -> assertNotEquals(certificate1, certificate5),
                () -> assertNotEquals(certificate1, certificate6),
                () -> assertNotEquals(certificate1, certificate7),
                () -> assertNotEquals(certificate1, certificate8),
                () -> assertNotEquals(certificate1, certificate9),
                () -> assertNotEquals(certificate3, certificate9)
        );
    }
}
